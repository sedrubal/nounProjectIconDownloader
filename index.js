"use strict";

console.log('HELLO FROM NOUN PROJECT ICON DOWNLOADER EXTENSION');

window.oldHref = document.location.href;
window.rendered = false;

function render() {
  const termNode = document.querySelector('[data-testid=title-block-container] > h1');
  const iconNode = document.querySelector('[data-testid=icon-editor-preview]');
  if (!termNode || !iconNode) {
    return;
  }
  const btn = document.createElement("a");
  const name = termNode.textContent.toLowerCase().trim();
  const data = window.getComputedStyle(iconNode).backgroundImage.split('"')[1];
  btn.name = `${name}.svg`;
  btn.download = `${name}.svg`;
  btn.href = data;
  btn.target = '_blank';
  btn.textContent = '⭳';
  btn.style.position = "absolute";
  btn.style.top = "5rem";
  btn.style.right = "1rem";
  btn.style.appearance = "none";
  btn.style.border = "none";
  btn.style.background = "#444";
  btn.style.color = "white";
  btn.style.width = "2.5rem";
  btn.style.height = "2.5rem";
  btn.style.borderRadius = "50%";
  btn.style.fontSize = "1.75rem";
  btn.style.padding = "0.5rem";
  iconNode.parentElement.parentElement.appendChild(btn);
  window.oldHref = window.location.href;
  window.rendered = true;
  console.log('Download button injected');
}

window.setInterval(() => {
  if (!window.oldHref || window.oldHref !== window.location.href) {
    window.rendered = false;
  }
  if (!window.rendered) {
    render();
  }
}, 1000);
